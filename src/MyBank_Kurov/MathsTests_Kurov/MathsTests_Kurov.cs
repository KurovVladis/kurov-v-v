﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using MyBank_Kurov;
using System.Collections.Generic;
using System.Reflection;

namespace BankDbTests_Kurov
{
    [TestClass]
    public class MathsTests_Kurov
    {
        [DataTestMethod]
        [DataRow(1, 1, 2)]
        [DataRow(2, 2, 4)]
        [DataRow(3, 3, 6)]
        [DataRow(0, 0, 1)] // The test run with this row fails
        public void AddIntegers_FromDataRowTest(int x, int y, int expected)
        {
            var target = new Maths_Kurov();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }


        public static IEnumerable<object[]> AdditionData
        {
            get
            {
                return new[]
                {
            new object[] { 1, 1, 2 },
            new object[] { 2, 2, 4 },
            new object[] { 3, 3, 6 },
            new object[] { 0, 0, 1 }, // The test run with this row fails
        };
            }
        }

        public static string GetCustomDynamicDataDisplayName(MethodInfo methodInfo, object[] data)
        {
            return string.Format("DynamicDataTestMethod {0} with {1} parameters", methodInfo.Name, data.Length);
        }

        [DynamicData(nameof(AdditionData), DynamicDataDisplayName = nameof(GetCustomDynamicDataDisplayName))]

        [TestMethod]
        
        public void AddIntegers_FromDynamicDataTest(int x, int y, int expected)
        {
            var target = new Maths_Kurov();
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }

        public TestContext TestContext { get; set; }

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", @"E:\TRPO\Kurov\Projects\MyBank_Kurov\MathsTests_Kurov\TestData_Kurov.csv", "TestData_Kurov#csv", DataAccessMethod.Sequential)]
        public void AddIntegers_FromDataSourceTest()
        {
            var target = new Maths_Kurov();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
