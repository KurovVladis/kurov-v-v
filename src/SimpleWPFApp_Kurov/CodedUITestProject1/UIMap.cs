﻿namespace CodedUITestProject1
{
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {

        /// <summary>
        /// ModifiedSimpleAppTest_Kurov - Используйте "SimpleAppTest_KurovParams" для передачи параметров в этот метод.
        /// </summary>
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }

        public void ModifiedSimpleAppTest_Kurov()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            #endregion

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(34, 5));

            uICheckBoxCheckBox.WaitForControlEnabled();

            // Выбор "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTest_KurovParams.UICheckBoxCheckBoxChecked;
        }

        public virtual SimpleAppTest_KurovParams SimpleAppTest_KurovParams
        {
            get
            {
                if ((this.mSimpleAppTest_KurovParams == null))
                {
                    this.mSimpleAppTest_KurovParams = new SimpleAppTest_KurovParams();
                }
                return this.mSimpleAppTest_KurovParams;
            }
        }

        private SimpleAppTest_KurovParams mSimpleAppTest_KurovParams;
    }
    /// <summary>
    /// Параметры для передачи в "ModifiedSimpleAppTest_Kurov"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class SimpleAppTest_KurovParams
    {

        #region Fields
        /// <summary>
        /// Выбор "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = true;
        #endregion
    }
}
